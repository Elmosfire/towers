from lxml import etree
from pathlib import Path


def get_best_solution(path):
    complexity = float("inf")
    res = None
    for x in path.glob("*.xml"):
        tree = etree.parse(x)
        local_complexity = int(tree.getroot().xpath("/puzzleset/complexity")[0].text)
        if local_complexity < complexity:
            complexity = local_complexity
            res = tree
    return res

def scan_solutions():
    root = etree.Element("root")
    for x in Path("cache").glob("*"):
        print(x.absolute())
        best = get_best_solution(x)
        root.append(best.getroot())
    root[:] = sorted(root[:], key=lambda child: (int(child.attrib["w"]), int(child.attrib.get("h")), str(child.attrib.get("distance"))))
        
    etree.ElementTree(root).write("solutions.xml", pretty_print=True)
        
if __name__ == "__main__":
    scan_solutions()
        