import numpy as np
from collections import Counter
from random import randint, shuffle
from tqdm import tqdm
from random import random, randrange
from itertools import count
from math import floor
from lxml import etree
import secrets
from pathlib import Path
from scan import scan_solutions
import z3

class Metric:
    @staticmethod
    def manhattan(d):
        def metric(relloc):
            return np.sum(relloc, axis=1) < d
        metric.__name__ = f"Manhattan {d}"
        return metric
    @staticmethod
    def euler(d):
        def metric(relloc):
            return np.sum(relloc**2, axis=1) < d**2
        metric.__name__ = f"Euler {d}"
        return metric
    @staticmethod
    def square(d):
        def metric(relloc):
            return np.max(relloc, axis=1) < d
        metric.__name__ = f"Square {d}"
        return metric
    @staticmethod
    def queen():
        def metric(relloc):
            return (relloc[:,0] - relloc[:,1]) * relloc[:,0] * relloc[:,1] == 0
        metric.__name__ = f"Queen"
        return metric


class Coloring:
    def __init__(self, layout, metric):
        self.layout = layout
        self.metric = metric
        
    @classmethod
    def from_validmatrix(cls, valid_fields, metric):
        lst = valid_fields.flatten()
        q = np.array([(i + 1) * bool(x) for i, x in enumerate(lst)])
        return cls(q.reshape(valid_fields.shape), metric)

    def count(self):
        c = Counter([y for y in self.layout.flatten() if y])
        if 0 in c:
            del c[0]
        return c

    def fitness(self):
        return sum(x * x for x in self.count().values())

    def locs(self, color):
        return np.array(list(np.where(self.layout == color))).T
    
    def relative_locs(self, x, y, color):
        pos = np.array([x, y])[..., :]
        return np.abs(self.locs( color) - pos)


    def any_color_in_range(self, x, y, color):
        return np.any(self.metric(self.relative_locs(x, y, color)))


    def all_colors_in_range(self, x, y, color):
        return np.all(self.metric(self.relative_locs(x, y, color)))

    def paint(self, x, y, color):
        new = self.layout.copy()
        new[x, y] = color
        return Coloring(new, self.metric)

    def nudge(self):
        flat = set(self.layout.flatten())
        for x in range(self.layout.shape[0]):
            for y in range(self.layout.shape[1]):
                if self.layout[x, y] == 0:
                    continue
                for color in flat:
                    if color == 0:
                        continue
                    if self.all_colors_in_range(x, y, color):
                        yield self.paint(x, y, color)


    def simplify(self):
        s = {k: i + 1 for i, k in enumerate(self.count())}
        s[0] = 0
        return Coloring(np.vectorize(s.get)(self.layout), None)


    def optimise(self):
        return max(self.nudge(), key=Coloring.fitness)
    
    def __hash__(self):
        return hash(self.layout.tobytes())


class Genoptimise:
    def __init__(self, validmatrix, metric):
        self.datastore = {}
        self.fitness = {}
        self.done = set()
        self.register(Coloring.from_validmatrix(validmatrix, metric))

    def register(self, coloring):
        key = hash(coloring)
        if key in self.datastore:
            return

        self.datastore[key] = coloring
        self.fitness[key] = (coloring.fitness(), random())

    def pop_opt(self):
        opt = max((x for x in self.fitness if x not in self.done), key=self.fitness.get)
        self.done.add(opt)
        return self.datastore[opt]

    def current_best(self):
        return self.datastore[max(self.fitness, key=self.fitness.get)].simplify()

    def do_step(self):
        for x in self.pop_opt().nudge():
            self.register(x)

    def improve(self, loops):
        currentbest = None
        stale = 0
        for _ in tqdm(count()):
            best = max(self.fitness, key=self.fitness.get)
            if best != currentbest:
                currentbest = best
                print(f"{best:x}", self.fitness[best], stale)
                stale = 0
            stale += 1
            try:
                self.do_step()
            except ValueError:
                pass
            if stale > loops:
                break


def excludeallsym(core):
    hflip = core | np.flip(core, axis=0)
    vflip = hflip | np.flip(hflip, axis=1)
    try:
        return vflip | vflip.T
    except ValueError:
        return vflip
    
def get_symmetries(core):
    for hflip in (core,np.flip(core, axis=0)):
        for vflip in (hflip,np.flip(hflip, axis=1)):
            yield vflip
            try:
                yield vflip.T
            except ValueError:
                pass

class TowerSys:
    colnames = [
        "grey",
        "red",
        "green",
        "blue",
        "yellow",
        "cyan",
        "magenta",
        "orange",
        "purple",
        "lime",
        "indigo",
        "gold",
        "aqua",
        "brown",
        "beige",
        "maroon",
        "olive",
        "darkblue",
        "lightblue",
        "Aquamarine",
        "Sea Turtle Green",
    ]
    colkeys = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    def __init__(self, towers, locdata, forceexclude, distance, coloring=None, name=(), latest_new=None, registry=...):

        self.locdata = locdata
        self.pure_exclude = np.zeros(locdata.shape, dtype=bool)
        self.forceexclude = forceexclude
        self.towers = towers
        self.distance = distance
        self.name = name
        self.to_place = towers - np.sum(locdata)
        self.solutions = []
        self.complexity = 1
        self.latest_new = latest_new
        
        if registry is ...:
            self.registry = {}
        else:
            self.registry = registry
        
        if np.any(self.locdata):
            for x in range(self.locdata.shape[0]):
                for y in range(self.locdata.shape[1]):
                    self.pure_exclude[x, y] = Coloring(self.locdata, self.distance).any_color_in_range(x, y, 1)
        
        self.exclude = self.pure_exclude | self.forceexclude
        if not np.any(locdata):
            self.exclude = excludeallsym(self.exclude)
        if not np.all(self.exclude):
            self.rloc = (randrange(locdata.shape[0]), randrange(locdata.shape[1]))
            while self.exclude[self.rloc]:
                self.rloc = (randrange(locdata.shape[0]), randrange(locdata.shape[1]))
        if coloring is None:
            self.coloring = self.build_coloring()
        else:
            self.coloring = Coloring((~self.exclude) * coloring.layout, self.distance)
            
        if self.symmetric_hash() not in self.registry:
            self.registry[self.symmetric_hash()] = self.name
            
    def symmetric_hash(self):
        mintow = min(map(lambda x: hash(x.tostring()), get_symmetries(self.locdata)))
        minex = min(map(lambda x: hash(x.tostring()), get_symmetries(self.exclude)))
        return (mintow, minex)
    
    def is_link(self):
        return self.registry[self.symmetric_hash()] != self.name

    def build_coloring(self):
        if np.all(self.exclude):
            return Coloring(np.zeros(self.locdata.shape, dtype=np.int8), self.distance)
        g = Genoptimise(~self.exclude, self.distance)
        g.improve(300)
        return g.current_best()

    def get_children(self, locations, applynone):
        newexclude = self.exclude.copy()
        for i, (x, y) in enumerate(locations):
            assert not self.exclude[x, y], "placing tower on an excluded field"
            newexclude[x, y] = True
        for i, (x, y) in enumerate(locations):
            t = self.locdata.copy()
            t[x, y] = True
            nval = TowerSys(
                self.towers, t, newexclude, self.distance, None, self.name + (i + 1,),
                (x,y),
                registry=self.registry,
            )
            assert np.sum(nval.exclude) > np.sum(
                self.exclude
            ), f"child has same freedom as parents, while putting tower on ({x},{y})"
            yield nval
        if applynone:
            if np.any(self.locdata):
                nval = TowerSys(
                    self.towers,
                    self.locdata.copy(),
                    newexclude,
                    self.distance,
                    self.coloring,
                    self.name + (i + 2,),
                    registry=self.registry,
                )
            else:
                nval = TowerSys(
                    self.towers,
                    self.locdata.copy(),
                    newexclude,
                    self.distance,
                    None,
                    self.name + (i + 2,),
                    registry=self.registry,
                )
            assert np.sum(nval.exclude) > np.sum(
                self.exclude
            ), "child has same freedom as parents, while exlusing colors"
            yield nval

    def best_coloring(self):
        c = self.coloring.count()
        try:
            return min(c, key=lambda x: c.get(x))
        except ValueError:
            return 0

    def get_abundance(self):
        return len(self.coloring.count()) - self.to_place

    @property
    def level(self):
        return 1 + len(self.name)
    
    def export_layout(self, relcolor=-1, markleft=None):
        source = etree.Element("layout", attrib=dict(
            width=str(self.locdata.shape[0]),
            height=str(self.locdata.shape[1]),
        ))
        for x in range(self.locdata.shape[0]):
            line = etree.SubElement(source, "line", attrib=dict(x=str(x)))
            for y in range(self.locdata.shape[1]):
                cell = etree.SubElement(line, "cell", attrib=dict(y=str(y)))
                if self.locdata[x, y]:
                    if (x,y) == self.latest_new:
                        etree.SubElement(cell, "tower", attrib=dict(mark="true"))
                    else:
                        etree.SubElement(cell, "tower", attrib=dict(mark="false"))
                elif self.pure_exclude[x,y]:
                    etree.SubElement(cell, "exclude", attrib=dict(type="tower"))
                elif self.forceexclude[x,y]:
                    etree.SubElement(cell, "exclude", attrib=dict(type="force"))
                elif self.exclude[x,y]:
                    etree.SubElement(cell, "exclude", attrib=dict(type="sym"))
                elif relcolor == self.coloring.layout[x, y] and markleft is None:
                    etree.SubElement(cell, "marked").text = str(self.coloring.layout[x, y])
                elif markleft is None:
                    etree.SubElement(cell, "color").text = str(self.coloring.layout[x, y])
                elif markleft == (x,y):
                    etree.SubElement(cell, "marked")
                else:
                    etree.SubElement(cell, "empty")
        return source
    
                    
    def export_layers(self):
        isdefault = str(self.latest_new is None).lower()
        
        
            
        abundance = self.get_abundance()
        colorlen = len(self.coloring.count())

        print("colors: ", self.coloring.count())

        best_coloring = self.best_coloring() if abundance >= 0 else -1
        tile_size = self.coloring.count().get(best_coloring, 0)
        name_as_str = '.'.join(map(str, self.name))
        
        root = etree.Element("case", attrib=dict(name=name_as_str, isdefault=isdefault))
        
        if abundance <= 0:
            root.append(self.export_layout(best_coloring))
        else:
            root.append(self.export_layout(-1, self.rloc))
        towers_left = etree.SubElement(root, "towers_left")
        towers_left.text = str(self.to_place)
        colorlen_ = etree.SubElement(root, "colors_count")
        colorlen_.text = str(colorlen)
                    
        if self.to_place == 0:
            if self.is_link():
                name_as_str2 = '.'.join(map(str, self.registry[self.symmetric_hash()]))
                etree.SubElement(root, "link", attrib=dict(name=name_as_str2))
            else:
                self.solutions = [name_as_str]
                etree.SubElement(root, "solution", attrib=dict(case=name_as_str))
            return root
        
        if colorlen == 0:
            return root
        
        
        if abundance < 0:
            etree.SubElement(root, "contradiction")
            return root
            
        elif abundance == 0:
            etree.SubElement(root, "exact")
                
        elif abundance > 0:
            etree.SubElement(root, "abundant")
            
        if self.is_link():
            name_as_str2 = '.'.join(map(str, self.registry[self.symmetric_hash()]))
            etree.SubElement(root, "link", attrib=dict(name=name_as_str2))
        else:
            for child in self.optimal_children():
                root.append(child.export_layers())  
                self.solutions.extend(child.solutions)
                self.complexity += child.complexity
            
        for x in self.solutions:
            etree.SubElement(root, "solution", attrib=dict(case=x))
        
        etree.SubElement(root, "complexity").text = str(self.complexity)
            
        return root
            
    def export_main(self, attrib):
        layers = self.export_layers()
        root = etree.Element("puzzle", attrib=attrib)
        root.append(layers)
        etree.SubElement(root, "complexity").text = str(self.complexity)
        for x in self.solutions:
            etree.SubElement(root, "solution", attrib=dict(case=x))
        return self.complexity, root

    def optimal_children(self):
        abundance = self.get_abundance()
        if abundance < 0:
            return
        elif abundance == 0:
            yield from self.get_children(
                self.coloring.locs(self.best_coloring()), False
            )
        else:
            yield from self.get_children(
                [self.rloc], True
            )

def inittower(w, h, towers, distance):
    return TowerSys(
        towers,
        np.zeros((w, h), dtype=np.bool),
        np.zeros((w, h), dtype=np.bool),
        distance,
        name=(),
    )
    
def get_max_number_of_towers(w, h, metric):
    
    grid = {}

    for x in range(w):
        for y in range(h):
            grid[x,y] = z3.Bool(f"g_({x},{y})")

    s = z3.Optimize()

    from itertools import product

    for x1,y1,x2,y2 in product(range(w), range(h), range(w), range(h)):
        relloc = np.array([[abs(x1-x2),abs(y1-y2)]])
        if not np.any(relloc):
            continue
        if np.all(metric(relloc)):
            s.add(z3.Not(z3.And(grid[x1,y1],grid[x2,y2])))

    s.maximize(sum(z3.If(x, 1, 0) for x in grid.values()))
    print(s)

    print(s.check())
    m = s.model()

    sol = np.zeros((w,h), dtype=np.bool)

    for x in range(w):
        for y in range(h):
            sol[x,y] = z3.is_true(m[grid[x,y]])
            
    print("opt solution", w, h, metric.__name__)
    print(sol)
    
    return np.sum(sol)
    

def optimise(w, h,  distance, trials=1):
    towers_max = get_max_number_of_towers(w, h, distance)
    puzzle_combo = etree.Element("puzzleset", dict(w=str(w), h=str(h), distance=distance.__name__))
    complexity = 0
    for towers in (towers_max, towers_max+1):
        complexity_, root = inittower(w, h, towers, distance).export_main(dict(w=str(w), h=str(h), towers=str(towers), distance=distance.__name__))
        puzzle_combo.append(root)
        complexity += complexity_
    etree.SubElement(puzzle_combo,"complexity").text = str(complexity)
    uniquename = secrets.token_urlsafe(22) 
    mname = distance.__name__.replace(' ',"_").lower()
    fname = Path(f"cache/{w}x{h}_{mname}/{uniquename}").with_suffix(".xml")
    fname.parent.mkdir(exist_ok=True, parents=True)
    etree.ElementTree(puzzle_combo).write(fname, pretty_print=True)

def sqmetricscan(l, h, distances):
    for s in range(l, h):
        for distance in distances:
            optimise(s, s, distance)

try:
    for _ in range(20):
        sqmetricscan(2,9, [Metric.manhattan(3),Metric.euler(3),Metric.manhattan(4),Metric.euler(4),Metric.queen()])

finally:
    scan_solutions()