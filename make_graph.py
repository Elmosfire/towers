from lxml import etree
from lxml.html import etree as htree
from pathlib import Path
import graphviz
from subprocess import run
from collections import namedtuple
import shutil
from itertools import chain

puzzles = [x for x in chain(*etree.parse("solutions.xml").getroot()) if x.tag=="puzzle"]


def get_puzzle_id(puzzle):
    return "{w}x{h}_{towers}_{distance}".format(**puzzle.attrib).replace(" ", "_").lower()

def get_case_id(puzzle, case):
    return get_puzzle_id(puzzle) + "/case_" + case.attrib["name"].replace(".", "_")

def case_id( case):
    return case.attrib["name"].replace(".", "_")

CellMap = namedtuple("CellMap", "empty,colors,marked,exclude,force,sym,tower")

class SvgCanvas:
    def __init__(self, puzzle, case):
        self.canvas = etree.fromstring("""                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="100" height="120" viewBox="0 0 100 120">
                        <defs>
                        </defs>
                        </svg>
                        """)
        
        self.puzzle = puzzle
        self.case = case
        try:
            self.width = int(case.xpath("layout")[0].attrib["width"])
            self.height = int(case.xpath("layout")[0].attrib["height"])
        except IndexError:
            print(case[:])
            raise
        
        
        
        self.rectangle(0, 20, 100, 100)
        if not case.xpath("link"):
            self.color_cells(CellMap(empty="white", colors="white", marked="lightgreen", exclude="lightgrey", force="orange", sym="cyan", tower="lightgrey"))
            self.annotate_cells(CellMap(empty="", colors=..., marked=..., exclude='', force="X", sym="S", tower=""))
        else:
            self.color_cells(CellMap(empty="white", colors="white", marked="white", exclude="lightgrey", force="orange", sym="cyan",tower="lightgrey"))
        self.rectangle(0, 0, 50, 20)
        self.rectangle(0, 0, 20, 20)
        self.text(case.xpath("colors_count")[0].text, 35, 20, 20)
        if case.xpath("contradiction"):
            self.text("D", 10, 20, 20)
        elif case.xpath("exact"):
            self.text("S", 10, 20, 20)
        elif case.xpath("abundant"):
            self.text("A", 10, 20, 20)
        self.rectangle(50, 0, 50, 20)
        self.text(case.xpath("towers_left")[0].text, 85, 20, 20)
        self.draw_tower_legend()
        
        self.draw_towers()
        if not case.xpath("link"):
            self.divide(CellMap(empty="empty", colors=..., marked="marked", exclude="exclude", force="force", sym="force",tower="exclude"))
        else:
            self.divide(CellMap(empty="none", colors="none", marked="none", exclude="exclude", force="force", sym="force",tower="exclude"))

        self.fname = (Path("drawings") / get_case_id(puzzle, case)).with_suffix(".svg")
        self.fname.parent.mkdir(exist_ok=True, parents=True)
        etree.ElementTree(self.canvas).write(self.fname, pretty_print=True , xml_declaration=True)
    def rectangle(self, x, y, w, h, col="white"):
        etree.SubElement(self.canvas,"rect", attrib=dict(
            x=str(x), 
            y=str(y), 
            width=str(w),
            height=str(h),
            stroke='black', 
            fill=col))
        
    def text(self, text, x, y, size, col="black"):
        etree.SubElement(self.canvas,"text", attrib={
            "font-size":str(size), 
            "text-anchor": "middle",
            "dy":"-.2em",
            "x":str(x),
            "y":str(y), 
            "fill":col}
        ).text = text
        
    def translate_x(self, x):
        return x * 100 / self.width
    
    def translate_y(self, y):
        return y * 100 / self.width + 20
    
    def path(self, points, stroke, fill):
        def find_paths():
            for t, x, y in points:
                yield f"{t}{self.translate_x(x)} {self.translate_y(y)}"
        etree.SubElement(self.canvas,"path", attrib=dict(d=" ".join(find_paths()), stroke=stroke, fill=fill))            
    
    def path_d(self, points, stroke, fill):
        def find_paths():
            for t, x, y in points:
                yield f"{t}{float(x)} {float(y)}"
        etree.SubElement(self.canvas,"path", attrib=dict(d=" ".join(find_paths()), stroke=stroke, fill=fill))    
    
    def border(self, x1, y1, x2, y2, color):
        self.path([("M", (x1+x2+y1-y2 + 1)/2, (x1-x2+y1+y2 + 1)/2), ("L", (x1+x2-y1+y2+1)/2, (-x1+x2+y1+y2+1)/2)], color, "none")
    
    def get_cell(self, x, y):
        return self.case.xpath(f"layout/line[@x='{x}']/cell[@y='{y}']")[0][0]
    
    def translate_cellmap(self, cellmap:CellMap, x, y):
        data = self.get_cell(x,y)
        if data.tag == "empty":
            return cellmap.empty
        elif data.tag == "color":
            if cellmap.colors == ...:
                return data.text
            else:
                return cellmap.colors
        elif data.tag == "marked":
            if cellmap.marked == ...:
                return data.text if data.text else ""
            else:
                return cellmap.marked
        elif data.tag == "exclude":
            if data.attrib.get("type")=="force":
                return cellmap.force
            elif data.attrib.get("type")=="sym":
                return cellmap.sym
            else:
                return cellmap.exclude
        elif data.tag == "tower":
            return cellmap.tower
        else:
            assert False, f"Invalid tag {data.tostring()}"
        
        
    def color_cells(self, cellmap: CellMap):
        for x in range(self.width):
            for y in range(self.height):
                    self.path([("M", x, y),("L", x+1, y), ("L", x+1, y+1),("L", x, y+1)], "lightgray", self.translate_cellmap(cellmap, x,y))
        self.path([("M", 0, 0),("L", self.width, 0), ("L", self.width, self.height),("L", 0, self.height), ("L", 0, 0)], "black", "none")
        
    def annotate_cells(self, cellmap: CellMap):
        for x in range(self.width):
            for y in range(self.height):
                self.text(str(self.translate_cellmap(cellmap, x, y)),  self.translate_x(x+0.5), self.translate_y(y+1), 100 / self.width, "grey")
    
    def divide(self, cellmap):
        for x in range(self.width-1):
            for y in range(self.height):
                if self.translate_cellmap(cellmap, x, y) != self.translate_cellmap(cellmap, x+1,y):
                    self.border(x, y, x+1, y, "black")
        for x in range(self.width):
            for y in range(self.height-1):
                if self.translate_cellmap(cellmap, x, y) != self.translate_cellmap(cellmap, x,y+1):
                    self.border(x, y, x, y+1, "black")         
                    
    def draw_tower(self, x, y, mark=False):
        cpos = [1/7+ x,5/6 + y]
        hist = [("M",) + tuple(cpos)]
        
        for x in "ruuluurdrurdrurddlddr":
            if x == "r":
                cpos[0] += 1/7
            elif x == "l":
                cpos[0] -= 1/7
            elif x == "u":
                cpos[1] -= 1/6
            elif x == "d":
                cpos[1] += 1/6
            hist.append(("L",) + tuple(cpos))
        if mark:
            self.path(hist, "red", "none")
        else:
            self.path(hist, "black", "none")
        
    def draw_tower_legend(self):
        cpos = [55,16]
        hist = [("M",) + tuple(cpos)]
        
        for x in "ruuluurdrurdrurddlddr":
            if x == "r":
                cpos[0] += 3
            elif x == "l":
                cpos[0] -= 3
            elif x == "u":
                cpos[1] -= 3
            elif x == "d":
                cpos[1] += 3
            hist.append(("L",) + tuple(cpos))
        self.path_d(hist, "black", "none")
        
    
        
    def draw_towers(self):
        for x in range(self.width):
            for y in range(self.height):
                if self.get_cell(x,y).tag == "tower":
                    self.draw_tower(x,y, self.get_cell(x,y).attrib["mark"] == "true")
    
output = htree.Element("div")
for puzzle in puzzles:
    print(puzzle)
    if int(puzzle.attrib["w"]) < 3:
        continue
    pid = get_puzzle_id(puzzle)
    with open(pid + ".sv", "w") as file:
        file.write("digraph { rankdir=\"LR\"")
        
        for case in puzzle.xpath('.//case'):
            id_ = case_id(case)
            #print(get_case_id(puzzle, case))
            sc = str(SvgCanvas(puzzle, case).fname)
            file.write(f""" "case{id_}" [label="" shape=none image="{sc}"];\n""")
            
        for case in puzzle.xpath('.//case'):
            id_ = case_id(case)
            for child in case.xpath("./case"):
                id_c = case_id(child)
                if child.attrib["isdefault"] == "true":
                    file.write(f""" "case{id_}" -> "case{id_c}" [arrowtail=box, dir=both];\n """)
                else:
                    file.write(f""" "case{id_}" -> "case{id_c}";\n """)
            for child in case.xpath("./link"):
                id_c = case_id(child)
                file.write(f""" "case{id_}" -> "case{id_c}" [arrowhead=ediamond, color=blue];\n """)

                       
        file.write("}")
    run(f"dot {pid}.sv -Tsvg -o {pid}.svg".split(" "))
    
    
    
    
    with open(f"{pid}.svg") as file:
        try:
            sroot = htree.parse(file).getroot()
            svg = sroot
        except IndexError:
            print(f"{pid}.svg")
            print(sroot.tag)
            raise
            
    rw = int(svg.attrib["width"][:-2])
    svg.attrib["width"] = f"{rw}px"
    del svg.attrib["height"]
    root = htree.Element("div")
    width = puzzle.attrib["w"]
    height = puzzle.attrib["h"]
    towers = int(puzzle.attrib["towers"])
    distance = puzzle.attrib["distance"]
    
    htree.SubElement(root, "h2").text = f"{width} by {height}, {towers} towers using metric {distance}."

    root.append(svg)
    
    if towers > 1 and len(puzzle.xpath("./solution")) < 5:
        output.append(root)
    
with open(f"intro.html") as file:
    root2 = htree.parse(file)

r = root2.getroot()
r.xpath("//*[@id='replace']")[0].append(output)

print(r.xpath("//*[@id='replace']"), list(r.xpath("//*[@id='replace']")) )

htree.ElementTree(r).write("public/index.html")
    
shutil.rmtree("public/drawings")
shutil.copytree("drawings", "public/drawings")
    
        
	


        
        

    
    
    